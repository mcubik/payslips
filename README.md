# Payslips

To run the server, execute:
```
ruby -Ilib ./bin/payslips
```

Example:

Get paylips
```
curl http://localhost:4567/payslips/201812 | jq .
```

Update paylips:
```
curl -X PUT http://localhost:4567/payslips/201812 -H 'Content-Type: application/json' --data '{"tax_rate": "7"}'
```
