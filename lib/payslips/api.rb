# frozen_string_literal: true

require 'sinatra/base'
require 'json'

module Payslips
  module API
    def self.new(app)
      Sinatra.new do
        before do
          content_type :json
        end

        get '/payslips/:month' do |month|
          year, month_num = parse_month(month)
          JSON.generate(app.payslips_for_month(year, month_num))
        rescue Payslips::InvalidRequest => e
          status 400
          e.message
        end

        put '/payslips/:month' do |month|
          year, month_num = parse_month(month)
          tax_rate = parse_tax_rate(request.body.read)
          app.update_tax_rate(year, month_num, tax_rate)
          status 200
        rescue Payslips::InvalidRequest => e
          status 400
          e.message
        end

        def parse_month(month)
          raise Payslips::InvalidRequest, 'Invalid month' if MONTH_FORMAT.match(month).nil?
          [month[0..3].to_i, month[4..5].to_i]
        end

        def parse_tax_rate(json)
          begin
            data = JSON.parse(json)
            BigDecimal(data['tax_rate'])
          rescue ArgumentError, TypeError, JSON::ParserError
            raise Payslips::InvalidRequest, 'Invalid percentage'
          end
        end
      end
    end

    private

    MONTH_FORMAT = /\d{6}/
  end
end
