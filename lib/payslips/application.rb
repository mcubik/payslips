# frozen_string_literal: true
require 'bigdecimal/util'

module Payslips
  # PayslipsApp
  # Handles the application use cases. It would be  
  # split in use case classes if the number of 
  # use cases increased.
  # Collaborators:
  # PayslipRepo - handles persistence
  # ReportLoader - loads payslips from a report
  class PayslipsApp
    def initialize(payslips_repo, report_loader)
      @payslips_repo = payslips_repo
      @report_loader = report_loader
    end

    def load
      @payslips_repo.save_all(@report_loader.load)
    rescue ImportError => e
      raise Error, e
    end

    def payslips_for_month(year, month)
      raise InvalidRequest unless valid_month?(year, month)
      @payslips_repo.find_by_month(year, month).map { |e| RespPayslip.for(e) }
    end

    def update_tax_rate(year, month, rate)
      # This could be very expensive with a big number of payslips
      # (in a real scenario, with real persistence)
      # Alternatives:
      # - Create a method for the bulk update in the repository
      # - Usa an object that represent the set, and can be updated
      # as a whole
      # - Etc.
      # Morover, this must be done inside a transaction.
      raise InvalidRequest unless valid_month?(year, month)
      begin
        @payslips_repo.save_all(@payslips_repo
          .find_by_month(year, month)
          .each { |e| e.update_tax_rate(rate) })
      rescue ArgumentError
        raise InvalidRequest, 'Invalid tax rate'
      end
    end

    private

    def valid_month?(year, month)
      month >=1 && month <= 12 && year > 1990
    end
  end

  # RespPayslip
  # Payslip response model generator
  # FIXME: imrove naming
  class RespPayslip
    def self.for(payslip)
      {
        id: payslip.id,
        vat: payslip.vat,
        date: payslip.date.strftime,
        gross: payslip.gross.to_digits,
        insurance: resp_percent(payslip.insurance),
        tax: resp_percent(payslip.tax),
        net: payslip.net.to_digits
      }
    end

    private

    def self.resp_percent(percent)
      {
        rate: percent.rate.to_digits,
        amount: percent.amount.to_digits
      }
    end
  end

  # Error
  # Root Payslips exception
  class Error < RuntimeError
  end

  # InvalidRequest
  # This exceptions is raised when the input to
  # a use case is invalid.
  class InvalidRequest < Error
  end

  # ImportError
  # Exception thrown when something goes wrong with
  # the importer
  class ImportError < Error
  end
end
