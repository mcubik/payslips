# frozen_string_literal: true

require 'bigdecimal'

module Payslips
  module Domain

    # Percentage
    # Represents a proportional value of a payslip.
    # Its value is expresed both as percentual and absolute.
    Percentage = Struct.new(:rate, :amount)

    # Payslip
    # Payslip business logic
    class Payslip
      attr_reader :id, :vat, :date, :gross, :insurance, :tax, :net

      def initialize(id, vat, date, gross, insurance, tax, net)
        @id = id
        @vat = vat
        @date = date
        @net = net
        @insurance = insurance
        @tax = tax
        @gross = gross
      end

      def update_tax_rate(rate)
        if rate + insurance.rate > BigDecimal('100') or rate < BigDecimal('0')
          raise ArgumentError
        end
        amount = @gross * rate / BigDecimal('100')
        @tax = Percentage.new(rate, amount)
        update_net
      end
    
      def hash
        @id.hash
      end

      def eql?(other)
        @id == other.id
      end

      private

      def update_net
        @net = @gross - @tax.amount - @insurance.amount
      end

    end
  end
end
