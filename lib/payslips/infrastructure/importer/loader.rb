require 'payslips/application'
require 'payslips/infrastructure/importer/parser'
require 'net/http'

module Payslips
  module Importer
    # ReportLoader
    # A report loader loads a set of payslips from a report
    # Collaborators:
    # ReportParser - Parses the contents of the report
    class ReportLoader

      def initialize(source)
        @source = source
      end

      def load
        Importer::ReportParser.parse(@source.get_contents)
      end

    end

    # RemoteReport
    # A report hosted in a remote HTTP server
    class RemoteReport
      def initialize(url)
        @url = url
      end

      # TODO: This method should return an object with a lazy / buffered behavior
      # in order to work correctly with large reports.
      # Returns the contents of the report 
      # Raises Payslips::Import error if it fails retrieving the report
      def get_contents
        begin
          res = Net::HTTP.get_response(URI(@url))
          # TODO: Chain error or log
          raise Payslips::ImportError, 'Error retrieving the report' unless res.is_a?(Net::HTTPSuccess)
          res.body
        rescue Exception => e
          raise Payslips::Error, 'Error retrieving the report'
        end
      end
    end
  end
end
