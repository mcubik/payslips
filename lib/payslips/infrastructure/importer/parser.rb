# frozen_string_literal: true

require 'payslips/domain'
require 'payslips/application'

module Payslips
  module Importer

    # LineParser
    # Extracts a payslip from a line of the report
    # Collaborators:
    # BigDecimal - Parses the amounts
    # Percentage - Parses the percentages
    class LineParser
      LINE_REGEX = /
        ^
        (?<id>\d{12})               # ID
        (?<vat>\w{9})               # VAT
        (?<date>\d{8})              # Date
        (?<gross>\d{8})             # Gross
        (?<insurance_rate>\d{4})    # Insurance rate
        (?<insurance_amount>\d{8})  # Insurance amount
        (?<tax_rate>\d{4})          # Tax rate
        (?<tax_amount>\d{8})        # Tax amount
        (?<net>\d{8})               # Net value
        $
      /x.freeze

      def self.parse(line)
        new(line).parse
      end

      private_class_method :new

      def initialize(line)
        @match = LINE_REGEX.match(line)
      end

      def parse
        raise Payslips::ImportError, 'Error parsing report' if @match.nil?

        Domain::Payslip.new(
          @match['id'], @match['vat'], parse_date, parse_gross,
          parse_insurance, parse_tax, parse_net
        )
      end

      private

      def parse_date
        date = Date.parse(@match['date'])
      rescue ArgumentError
        raise Payslips::ImportError, 'Invalid date'
      end

      def parse_gross
        BigDecimal.parse(@match['gross'])
      end

      def parse_insurance
        Domain::Percentage.parse(
          @match['insurance_rate'], @match['insurance_amount']
        )
      end

      def parse_tax
        Domain::Percentage.parse(
          @match['tax_rate'], @match['tax_amount']
        )
      end

      def parse_net
        BigDecimal.parse(@match['net'])
      end
    end

    # ReportParser
    # Parses the contents of a payslip report
    # Collaborators:
    # LineParser - parses single lines
    class ReportParser
      def self.parse(report)
        new(report).parse
      end

      private_class_method :new

      def initialize(report)
        @report = report
      end

      def parse
        @report.lines.map { |l| LineParser.parse(l) }
      end
    end

    # This is not optiomal, as the added methods become
    # global (unlike languages like Rust which limit the 
    # scope of type extensions).
    # The alternative would have been to use a class method.


    class ::BigDecimal
      DECIMALS = 2
      def self.parse(s)
        int = s[0..s.size - DECIMALS - 1]
        dec = s[s.size - DECIMALS..-1]
        BigDecimal("#{int}.#{dec}")
      end
    end

    class Domain::Percentage
      def self.parse(rate, amount)
        new(BigDecimal.parse(rate), BigDecimal.parse(amount))
      end
    end
  end
end
