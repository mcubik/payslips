# frozen_string_literal: true

module Payslips
  module PayslipRepo
    # InMemory
    # In-memory implementation of the payslip repository.
    # It just saves copies of the payslips.
    class InMemory
      def initialize(payslips = [])
        @payslips = {}
        save_all(payslips)
      end

      def save_all(payslips)
        payslips.each { |e| save(e) }
      end

      def find_by_month(year, month)
        @payslips.fetch([year, month], {}).values.map(&:clone)
      end

      def save(payslip)
        month = [payslip.date.year, payslip.date.month]
        @payslips[month] = {} unless @payslips.key?(month)
        @payslips[month][payslip.id] = payslip.clone
      end

      def count
        @payslips.values.map(&:size).sum
      end
    end
  end
end
