require 'payslips/api'
require 'payslips/application'
require 'payslips/infrastructure/repositories'
require 'payslips/infrastructure/importer/loader'

module Payslips

  REPORT_URL = 'https://gist.githubusercontent.com/rodrigm/bf42bf2aa0f1d17381b412a3ffec7fd9/raw/3997a797e334d19e8ef3fb853cf474b22237522d/payslips.201812.txt'

  app = PayslipsApp.new(
    PayslipRepo::InMemory.new,
    Importer::ReportLoader.new(
      Importer::RemoteReport.new(Payslips::REPORT_URL)))
  app.load()
  API.new(app).run!
end
