# frozen_string_literal: true

require File.expand_path '../unit_test_helper.rb', __dir__
require 'rack/test'
require 'payslips/api'
require 'json'
require 'date'

RSpec.describe Payslips::API do
  include Rack::Test::Methods

  let(:ps1) { Payslips::RespPayslip.for(a_payslip(id: '1')) }
  # This is confusing but rack-test makes me use this name for the Sinatra server
  let(:app) { Payslips::API.new(application) }
  let(:application) { double(payslips_for_month: [ps1]) }

  it 'lists the payslips of a month' do
    get '/payslips/201901'
    expect(last_response.ok?).to be(true)
    expect(last_response.headers['Content-Type']).to eq('application/json')
    payslips = JSON.parse(last_response.body)
    expect(payslips).to have(1).item
    expect(payslips.first).to include('id' => '1', 'gross' => '2486.0')
  end

  it 'fail if month is invalid' do
    get '/payslips/10xxxx'
    expect(last_response.status).to eq(400)
  end

  it 'updates the tax rate for a month' do
    expect(application).to receive(:update_tax_rate)
      .with(2019, 1, BigDecimal('95'))
    put 'payslips/201901', '{"tax_rate": 95}'
    expect(last_response.ok?).to be(true)
  end

  context 'invalid data for update tax rate' do
    it 'fails the month is invalid' do
      put 'payslips/20xxx', '{"tax_rate": 95}'
      expect(last_response.status).to eq(400)
    end

    it 'fails if tax rate is missing' do
      put 'payslips/201901'
      expect(last_response.status).to eq(400)
    end

    it 'fails if tax rate is not a number' do
      put 'payslips/201901', '{"tax_rate": "aaa"}'
      expect(last_response.status).to eq(400)
    end

    # TODO: A lot more to test here

  end


end
