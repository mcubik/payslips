# frozen_string_literal: true

require 'payslips/application'
require 'payslips/infrastructure/repositories'

RSpec.describe Payslips::PayslipsApp do
  let(:ps1) { a_payslip(id: '1', date: Date.new(2019, 1, 20)) }
  let(:ps2) { a_payslip(id: '2', date: Date.new(2019, 1, 21)) }
  let(:repo) { Payslips::PayslipRepo::InMemory.new }
  let(:loader) { double('loader', load: [ps1, ps2]) }
  let(:app) { described_class.new(repo, loader) }

  before do
    app.load
  end

  it 'returns the payslips for a given month' do
    expect(app.payslips_for_month(2019, 1)).to have(2).items.and(
      include(
        include(id: ps1.id),
        include(id: ps2.id)
      )
    )
  end

  it 'loads the payslips' do
    expect(repo.count).to be(2)
  end

  it 'returns an error if import fails' do
    allow(loader).to receive(:load).and_raise(Payslips::ImportError)
    expect { app.load }.to raise_error(Payslips::Error)
  end

  context 'invalid input' do
    it 'fails if the month is invalid' do
      expect { app.payslips_for_month(2019, 13) }.to(
        raise_error(Payslips::InvalidRequest)
      )

      expect { app.payslips_for_month(2019, 0) }.to(
        raise_error(Payslips::InvalidRequest)
      )

      expect { app.payslips_for_month(1, 12) }.to(
        raise_error(Payslips::InvalidRequest)
      )
    end
  end

  it 'updates the payslips tax rate of aonth' do
    app.update_tax_rate(2019, 1, BigDecimal('95'))
    expect(app.payslips_for_month(2019, 1)).to have(2).items.and(
      include(
        include(net: '0.0'),
        include(net: '0.0')
      )
    )
  end

end
