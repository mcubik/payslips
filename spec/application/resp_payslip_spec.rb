# frozen_string_literal: true
require 'bigdecimal/util'
require 'payslips/application'

RSpec.describe Payslips::RespPayslip do
  let(:payslip) { a_payslip(id: '1', date: Date.new(2019, 0o1, 20)) }

  it 'serializes a payslip' do
    expect(described_class.for(payslip)).to eq(
      id: payslip.id,
      vat: payslip.vat,
      date: payslip.date.strftime,
      gross: payslip.gross.to_digits,
      insurance: {
        rate: payslip.insurance.rate.to_digits,
        amount: payslip.insurance.amount.to_digits
      },
      tax: {
        rate: payslip.tax.rate.to_digits,
        amount: payslip.tax.amount.to_digits
      },
      net: payslip.net.to_digits
    )
  end
end
