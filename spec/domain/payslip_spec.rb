# frozen_string_literal: true

require 'payslips/domain'
require 'date'

RSpec.describe Payslips::Domain::Payslip do
  let(:id) { '000000000001' }
  let(:vat) { '97084172E' }
  let(:gross) { BigDecimal('2486') }
  let(:date) { Date.new(2018, 12, 31) }
  let(:insurance) do
    Payslips::Domain::Percentage.new(BigDecimal('5', 2), BigDecimal('124.30'))
  end
  let(:tax) do
    Payslips::Domain::Percentage.new(BigDecimal('12'), BigDecimal('289.32'))
  end
  let(:net) { BigDecimal('2063.37') }

  let(:payslip) { a_payslip(id: '1234') } 

  it 'is initialized with the payslip data' do
    expect(described_class.new(id, vat, date, gross, insurance, tax, net)).to(
      have_attributes(
        id: id,
        vat: vat,
        date: date,
        net: net,
        insurance: insurance,
        tax: tax,
        gross: gross
      )
    )
  end

  it 'defines hash equality by id' do
    another_payslip = a_payslip(id: '1234', gross: BigDecimal('2000'))
    expect(payslip.eql?(another_payslip)).to be(true)
  end

  it 'can change the tax rate' do
    payslip.update_tax_rate(BigDecimal('10'))
    # Gross 2486, Insurance: 124.30 -> 2482 - 124.30 - 248.60 = 2358 = 2113.10
    expect(payslip).to have_attributes(
        tax: a_percentage('10.00', '248.60'),
        net: BigDecimal('2113.10')
    )
  end

  it 'fails to update tax rate if tax + insurance > 100%' do
    # Ok
    expect { payslip.update_tax_rate(BigDecimal('95.00')) }.to(
      change { payslip.net }.from(BigDecimal('2063.37')).to(BigDecimal('0')))
    # Bad
    expect { payslip.update_tax_rate(BigDecimal('96.00')) }.to(
      raise_error(ArgumentError))
  end

  it 'fails to update tax rate rate is negative' do
    expect { payslip.update_tax_rate(BigDecimal('-1')) }.to(
      raise_error(ArgumentError))
  end

end
