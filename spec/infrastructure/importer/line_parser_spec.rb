# frozen_string_literal: true

require File.expand_path '../../unit_test_helper.rb', __dir__
require 'payslips/infrastructure/importer/parser'
require 'payslips/application'

RSpec.describe Payslips::Importer::LineParser do
  let(:line) do
    '00000000000197084172E201812310024860005000001243012000002983200206337'
  end

  it 'parses a payslip from a report line' do
    expect(described_class.parse(line)).to(
      be_a(Payslips::Domain::Payslip).and(
        have_attributes(
          id: '000000000001',
          vat: '97084172E',
          date: Date.new(2018, 12, 31),
          gross: BigDecimal('2486.00'),
          insurance: a_percentage('05.00', '124.30'),
          tax: a_percentage('12.00', '298.32'),
          net: BigDecimal('2063.37')
        )
      )
    )
  end

  context 'with invalid data' do
    it 'fails if the record size is invalid' do
      expect do
        described_class.parse(line[0..line.size - 2])
      end.to raise_error(Payslips::ImportError)
    end

    # I'm testing only one field. I could easly test the validation of every field
    it 'fails if the line format is invalid' do
      expect do
        described_class.parse(line.replace_at(0, '0000000000A1'))
      end.to raise_error(Payslips::ImportError)
    end

    it 'fails if date is invalid' do
      expect do
        described_class.parse(line.replace_at(21, '20180231'))
      end.to raise_error(Payslips::ImportError)
    end
  end
end
