# frozen_string_literal: true

require File.expand_path '../../unit_test_helper.rb', __dir__
require 'payslips/infrastructure/importer/parser'
require 'payslips/application'

RSpec.describe Payslips::Importer::ReportParser do
  let(:report) do
    <<~end
      00000000000197084172E201812310024860005000001243012000002983200206337
      00000000039745201136H201812310025280005000001264010000002528000214879
    end
  end

  it 'returns an empty list when the report is empty' do
    expect(described_class.parse('')).to eq([])
  end

  it 'parses a payslip report' do
    # With Hamcrest (in Python) I can do all the assertions inside the
    # collection assertion, using all_of, etc. That reads better than
    # the following, but I don't know how to do so with RSpec.
    expect(described_class.parse(report)).to have(2).items.and(
      include(
        be_a(Payslips::Domain::Payslip).and(have_attributes(id: '000000000001')),
        be_a(Payslips::Domain::Payslip).and(have_attributes(id: '000000000397'))
      )
    )
  end

  context 'with invalid data' do
    it 'fails if the report format is invalid' do
      expect do
        described_class.parse(report.replace_at(0, '0_0'))
      end.to raise_error(Payslips::ImportError)
    end
  end
end
