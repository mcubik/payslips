# frozen_string_literal: true

require File.expand_path '../../unit_test_helper.rb', __dir__
require 'payslips/infrastructure/repositories'

RSpec.describe Payslips::PayslipRepo::InMemory do
  let(:payslip_1) { a_payslip(id: '9', date: Date.new(2018, 12, 31)) }
  let(:payslip_2) { a_payslip(id: '10', date: Date.new(2019, 1, 1)) }
  let(:payslip_3) { a_payslip(id: '11', date: Date.new(2019, 1, 2)) }

  let(:repo) { described_class.new([payslip_1, payslip_2, payslip_3]) }

  it 'stores a payslips' do
    described_class.new.save(payslip_1)
    expect(repo.find_by_month(2018, 12)).to have(1).items.and(
      include(
        be_a(Payslips::Domain::Payslip),
        have_attributes(id: '9')
      )
    )
  end

  it 'retrieves payslips by month' do
    expect(repo.find_by_month(2018, 12)).to have(1).items.and(
      include(
        have_attributes(id: '9')
      )
    )
    expect(repo.find_by_month(2019, 1)).to have(2).items.and(
      include(
        have_attributes(id: '10'),
        have_attributes(id: '11')
      )
    )
  end

  it 'updates a payslip' do
    payslip_1.update_tax_rate(BigDecimal('95'))
    repo.save(payslip_1)
    expect(repo.find_by_month(2018, 12).first.net).to eq(BigDecimal('0'))
  end

end
