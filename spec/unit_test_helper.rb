# frozen_string_literal: true

require 'bigdecimal'
require 'payslips/domain'
require 'date'

def a_percentage(rate, amount)
  Payslips::Domain::Percentage.new(BigDecimal(rate), BigDecimal(amount))
end

def a_payslip(**attrs)
  args = DEFAULT_PAYSLIP_ATTRS.merge(attrs)
  Payslips::Domain::Payslip.new(
    args[:id], args[:vat], args[:date], args[:gross],
    args[:insurance], args[:tax], args[:net]
  )
end

class String
  def replace_at(index, str)
    self[0..[0, index - 1].max] + str + self[index + str.size..-1]
  end
end

DEFAULT_PAYSLIP_ATTRS = {
  id: '000000000001',
  vat: '97084172E',
  date: Date.new(2018, 12, 31),
  gross: BigDecimal('2486.00'),
  insurance: a_percentage('05.00', '124.30'),
  tax: a_percentage('12.00', '298.32'),
  net: BigDecimal('2063.37')
}.freeze
